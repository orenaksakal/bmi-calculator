/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var result = 0;
function calculate() {
    var age = $('#age').val();
    var weight = $('#weight').val();
    var height = $('#height').val() * 0.01;
    // male - female
    var gender = $('#gender').val();
    if (age == '' || weight == '' || height == '') {
        alert("Please fill all form elements");
    } else {
        var formula = weight / (height * height);

        if (formula < 18.5) {
            // UnderWeight
            result = "You Are Under Weight";

        } else if (formula >= 18.5 && formula < 24.9) {
            // Normal 
            result = "You Are Normal Weighted";

        } else if (formula >= 25 && formula < 29.9) {
            // Overweight
            result = "You Are Over Weight";
        } else if (formula >= 30) {
            // Obese
            result = "You Are Obese";
        }
        return popupResult(result);
    }
};
function popupResult(result) {
    document.getElementById("result").innerHTML = result;
};

